# IGWN Credmon Vault configuration {: #41-vault-credmon-igwn }

## Overview

!!! info "Basic info"
    **Filename**: `41-vault-credmon-igwn.conf`  
    **Roles**: `submit`  
    **Package**: `igwn-htcondor-config-vault`

The `41-vault-credmon-igwn.conf` configuration configures
the `condor_credmon_vault` daemon service to talk to the
IGWN vault when getting tokens for jobs.

## Details

The `41-vault-credmon-igwn.conf` augments the
`40-vault-credmon.conf` configuration provided by the
`condor-credmon-vault` RPM by setting `SEC_CREDENTIAL_GETTOKEN_OPTS`
to tell [`htgettoken`](https://github.com/fermitools/htgettoken)
to use the `--vaultserver vault.ligo.org` argument.

!!! tip "Debian implementation"
    On Debian, there is no separate package that includes the
    `40-vault-credmon.conf` configuration, so
    `igwn-htcondor-config-vault` `Depends` on `htcondor-doc` and then
    symlinks `40-vault-credmon.conf` from the documented examples
    (in `/usr/share/doc/condor`) into `/etc/condor/config.d`.

## Raw configuration file {: #raw }

```text
{%
  include-markdown "../config/41-vault-credmon-igwn.conf"
  comments=false
%}
```
