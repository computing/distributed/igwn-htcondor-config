# Default `request_memory` value {: #99-memory }

## Overview

!!! info "Basic info"
    **Filename**: `99-memory.conf`  
    **Roles**: `submit`  
    **Package**: `igwn-htcondor-config-jobrequirements`

The `99-memory.conf` configuration sets a default value for
the `request_memory` submit command.

## Raw configuration file {: #raw }

```text
{%
  include-markdown "../config/99-memory.conf"
  comments=false
%}
```
