# Disable DAGMan direct submission {: #93-dagman-use-direct-submit }

## Overview

!!! info "Basic info"
    **Filename**: `93-dagman-use-direct-submit.conf`  
    **Roles**: `submit`  
    **Package**: `igwn-htcondor-config-dagman`

The `93-dagman-use-direct-submit.conf` configuration disables the use of
[direct submission](https://htcondor.readthedocs.io/en/lts/admin-manual/configuration-macros.html#:~:text=DAGMAN_USE_DIRECT_SUBMIT)
by `condor_dagman`.

This is to workaround an bug in HTCondor related to using OAuth tokens with
jobs in DAG workflows.
For details see <https://github.com/lscsoft/osg-igwn/issues/489#issuecomment-1505836355>.

## Raw configuration file {: #raw }

```text
{%
  include-markdown "../config/93-dagman-use-direct-submit.conf"
  comments=false
%}
```
