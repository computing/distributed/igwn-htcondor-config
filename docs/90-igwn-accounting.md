# Enforced IGWN accounting tags {: #90-igwn-accounting }

## Overview

!!! info "Basic info"
    **Filename**: `90-igwn-accounting.conf`  
    **Roles**: `submit`  
    **Package**: `igwn-htcondor-config-accounting`

The `90-igwn-accounting.conf` configuration enforces that jobs are
submitted with valid IGWN accounting information.

## Details

### Submit requirements

The `90-igwn-accounting.confg` file provides HTCondor job transforms
to enforce that submitted jobs include the following attributes:

| Attribute | Required | Purpose |
| --------- | -------- |------- |
| `accounting_group` {: .nowrap } | Always | The tag to use for this job when accounting |
| `accounting_group_user` {: .nowrap } | For shared accounts | The name of the user to associated this job to when accounting |

The values of the two attributes must match entries in the following files

| Attribute | Map file |
| --------- | -------- |
| `accounting_group` {: .nowrap } | `/etc/condor/accounting/valid_tags` |
| `accounting_group_user` {: .nowrap } | `/etc/condor/accounting/valid_users` |

!!! warning "The generation of the map files is not automated by this configuration"

    The generation of the two map files is not automated or associated in
    any way with this configuration, and is left to the administrator of
    the HTCondor submit host to handle as they wish.

### Job transforms

The two accounting attributes are then stored in the Job ClassAd as follows

| Submit command | Job Ad |
| -------------- | ------ |
| `accounting_group` | `LigoSearchTag` |
| `accounting_group_user` | `LigoSearchUser` |

### Exceptions

The following job types are exempt:

- [Scheduler Universe](https://htcondor.readthedocs.io/en/latest/users-manual/choosing-an-htcondor-universe.html#scheduler-universe) jobs (`JobUniverse == 7`)
- [Glideins](https://htcondor.readthedocs.io/en/latest/grid-computing/introduction-grid-computing.html)

## Raw configuration file {: #raw }

```text
{%
  include-markdown "../config/90-igwn-accounting.conf"
  comments=false
%}
```
