# `SYSTEM_PERIODIC_HOLD` {: #65-system-periodic-hold }

## Overview

!!! info "Basic info"
    **Filename**: `65-system-periodic-hold.conf`  
    **Roles**: `submit`  
    **Package**: `igwn-htcondor-config-periodichold`

The `65-system-periodic-hold.conf` configuration improves the
user experience around held jobs by configuring `SYSTEM_PERIODIC_HOLD`
expressions.

## Raw configuration file {: #raw }

```text
{%
  include-markdown "../config/65-system-periodic-hold.conf"
  comments=false
%}
```
