# Configuration distribution

The IGWN HTCondor Configurations are distributed by IGWN in the
Debian and RHEL software repositories.
For details of how to configure those for your system, see
[here](https://computing.docs.ligo.org/guide/software/).

## Per-configuration packages {: #config }

Each configuration is separately distributed in a binary package
with the prefix `igwn-htcondor-config` and a suffix relating to
the configuration it provides, e.g.

```text
igwn-htcondor-config-accounting
```

See the links in the navigation menu for detailed descriptions of
each configuration.

## Role metapackages {: #role }

The following meta-packages are distributed to simplify top-level
configuration of a host to an HTCondor role:

| Package | Role |
| ------- | ---- |
| `igwn-htcondor-config-submit` {: .nowrap } | HTCondor submit node |
| `igwn-htcondor-config-execute` {: .nowrap } | HTCondor execute node |
