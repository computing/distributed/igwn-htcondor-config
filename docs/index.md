# IGWN HTCondor Configurations

The IGWN HTCondor Configurations project maintains and documents the
various customised configurations used to operate the IGWN
HTCondor DHTC platform.

## Details

See the links in the navigation menu for detailed descriptions of
each configuration.

## Distributions

See [_Distributions_](./distribution.md) for details on how the
IGWN HTCondor Configurations are bundled and distributed.
