# Mandatory `request_disk` value {: #99-request-disk }

## Overview

!!! info "Basic info"
    **Filename**: `99-request-disk.conf`  
    **Roles**: `submit`  
    **Package**: `igwn-htcondor-config-jobrequirements`

The `99-request-disk.conf` configuration displays a warning
for jobs that are submitted without specifying `request_disk`,
and then places running jobs on old if they didn's specify
`request_disk` and then exceeded 1GB.

## Details

### Future plans

In the future, the warning will be migrated to a hard error, thus
requiring all jobs to be submitted with `request_disk` specified.

## Raw configuration file {: #raw }

```text
{%
  include-markdown "../config/99-request-disk.conf"
  comments=false
%}
```
