%define srcname igwn-htcondor-config
%define version 20240423
%define release 1

Name: %{srcname}
Version: %{version}
Release: %{release}%{?dist}
Summary: HTCondor configuration for IGWN
License: Apache-2.0
Url: https://git.ligo.org/computing/distributed/igwn-htcondor-config
Source0: https://software.igwn.org/sources/source/%{srcname}-%{version}.tar.gz
Packager: Duncan Macleod <duncan.macleod@ligo.org>
Prefix: %{_prefix}

# build
BuildArch: noarch

# -- packages ---------------

# -- igwn-htcondor-config

Requires: %{name}-execute = %{version}-%{release}
Requires: %{name}-submit = %{version}-%{release}

%description
IGWN-specific configuration files for HTCondor

This metapackage includes all configuration files across
all roles.

%files
%doc README.md
%license LICENSE

# -- igwn-htcondor-config-submit

%package submit
Summary: HTCondor configuration for IGWN Access Points
Requires: %{name}-accounting = %{version}-%{release}
Requires: %{name}-dagman = %{version}-%{release}
Requires: %{name}-jobresources = %{version}-%{release}
Requires: %{name}-periodichold = %{version}-%{release}
Requires: %{name}-vault = %{version}-%{release}

%description submit
All IGWN-specific configuration files for the HTCondor Submit role.

%files submit
%doc README.md
%license LICENSE

# -- igwn-htcondor-config-execute

%package execute
Summary: HTCondor configuration for IGWN Execute hosts

%description execute
All IGWN-specific configuration files for the HTCondor Execute role.

%files execute
%doc README.md
%license LICENSE

# -- accounting

%package accounting
Summary: HTCondor configuration for IGWN Accounting

%description accounting
IGWN Usage Accounting configuration for HTCondor

%files accounting
%doc README.md
%license LICENSE
%config(noreplace) %{_sysconfdir}/condor/config.d/90-igwn-accounting.conf

# -- dagman

%package dagman
Summary: HTCondor configuration for IGWN and DAGMan

%description dagman
IGWN DAGMan configuration for HTCondor

%files dagman
%doc README.md
%license LICENSE
%config(noreplace) %{_sysconfdir}/condor/config.d/93-dagman-use-direct-submit.conf

# -- jobresources

%package jobresources
Summary: HTCondor configuration for IGWN job resource requirements

%description jobresources
IGWN HTCondor configuration for job resource requirements, including
mandatory request_disk and a default request_memory value.

%files jobresources
%doc README.md
%license LICENSE
%config(noreplace) %{_sysconfdir}/condor/config.d/99-memory.conf
%config(noreplace) %{_sysconfdir}/condor/config.d/99-request-disk.conf

# -- periodic-hold

%package periodichold
Summary: HTCondor configuration for IGWN SYSTEM_PERIODIC_HOLD

%description periodichold
IGWN configuration for HTCondor's SYSTEM_PERIODIC_HOLD expression

%files periodichold
%doc README.md
%license LICENSE
%config(noreplace) %{_sysconfdir}/condor/config.d/65-system-periodic-hold.conf

# -- vault

%package vault
Summary: %{summary} Vault
Requires: condor-credmon-vault

%description vault
IGWN CredMon Vault configuration for HTCondor

%files vault
%doc README.md
%license LICENSE
%config(noreplace) %{_sysconfdir}/condor/config.d/41-vault-credmon-igwn.conf

# -- build ------------------

%prep
%autosetup -n %{srcname}-%{version}

%build

%install
mkdir -p %{buildroot}%{_sysconfdir}/condor/config.d/
%__install -m 644 -p -v config/*.conf %{buildroot}%{_sysconfdir}/condor/config.d/

# -- changelog --------------

%changelog
* Wed Apr 17 2024 Duncan Macleod <duncan.macleod@ligo.org> - 20240417-1
- April 2024 config update

* Fri Jun 24 2022 Duncan Macleod <duncan.macleod@ligo.org> - 20220624-1
- First release with accounting configuration
