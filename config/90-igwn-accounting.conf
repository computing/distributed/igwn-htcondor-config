# -- IGWN HTCondor configuration for job accounting
# this config restricts job submission to those
# jobs for which the accounting_group and
# accounting_group_user match a predefined list of
# acceptable values

JOB_TRANSFORM_NAMES = $(JOB_TRANSFORM_NAMES) TagJob RemoveAcctGroup

# replace the AcctGroup and AcctGroupUser with LIGO-specific
# attributes
JOB_TRANSFORM_TagJob @=end
[
  COPY_AcctGroup = "LigoSearchTag";
  COPY_AcctGroupUser = "LigoSearchUser";
  EVAL_SET_LigoSearchTag = LigoSearchTag ?: "None";
  EVAL_SET_LigoSearchUser = LigoSearchUser ?: Owner;
]
@end

# do not strip accounting classads from scheduler universe
# because their presence is necessary to propagate to child
# jobs and sub-DAGs
JOB_TRANSFORM_RemoveAcctGroup @=end
[
  REQUIREMENTS = JobUniverse != 7;
  DELETE_AccountingGroup = True;
  DELETE_AcctGroup = True;
  DELETE_AcctGroupUser = True;
]
@end

# define userMaps to validate the accounting tag and user
SCHEDD_CLASSAD_USER_MAP_NAMES = $(SCHEDD_CLASSAD_USER_MAP_NAMES) ValidSearchTags ValidSearchUsers
CLASSAD_USER_MAPFILE_ValidSearchTags = /etc/condor/accounting/valid_tags
CLASSAD_USER_MAPFILE_ValidSearchUsers = /etc/condor/accounting/valid_users

SUBMIT_REQUIREMENT_NAMES = $(SUBMIT_REQUIREMENT_NAMES) ValidateSearchTag ValidateSearchUser

# validate the accounting_group entry; returns true if
# - this is a DAGMan job
# - OR accounting_group maps to a valid tag
# - OR this was submitted by an osgXY (pilot) user
# - OR job is a glidein (pilot)
SUBMIT_REQUIREMENT_ValidateSearchTag = \
  JobUniverse == 7 || \
  userMap("ValidSearchTags", LigoSearchTag) isnt undefined || \
  regexp("osg[0-9]+", Owner) || \
  GlideinClient isnt undefined
SUBMIT_REQUIREMENT_ValidateSearchTag_REASON = \
  strcat("Invalid value for search tag: ",LigoSearchTag ?: "<undefined>", "\n", \
         "  See https://accounting.ligo.org/user for guidance on adding LIGO tags or the\n", \
         "  file /etc/condor/accounting/valid_tags for those currently accepted here.")

# validate the accounting_group_user entry; returns true if
# - this is a DAGMan job
# - OR submitter (Owner) is a valid user
# - OR accounting_group_user was given and is valid
# - OR this was submitted by an osgXY (pilot) user
# - OR this is a glidein (pilot) job
SUBMIT_REQUIREMENT_ValidateSearchUser = \
  JobUniverse == 7 || \
  userMap("ValidSearchUsers", Owner, LigoSearchUser) is LigoSearchUser || \
  userMap("ValidSearchUsers", Owner) is undefined && Owner =?= LigoSearchUser || \
  regexp("osg[0-9]+", Owner) || \
  GlideinClient isnt undefined
SUBMIT_REQUIREMENT_ValidateSearchUser_REASON = \
  strcat("Invalid value for search user: ", LigoSearchUser ?: "<undefined>", "\n", \
         "       Valid values are: ",userMap("ValidSearchUsers", Owner))
