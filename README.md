# IGWN HTCondor Configuration

This project contains HTCondor configuration files used by IGWN computing
centres to provide common functionality across all centres.

The configuration files are managed in the `config/` sub-directory, and
should be internally documented.

Contributions are always welcome!
Please review the [contributing guide](CONTRIBUTING.md) prior to your first
contribution.
