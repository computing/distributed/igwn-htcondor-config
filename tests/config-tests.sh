#!/bin/bash
#
# Tests for igwn-htcondor-config
#

# -- configuration

# error quickly
set -e

# count failures
failures=0

# -- functions

assert_condor_config_val() {
  local _var=$1
  local _value=$2
  shift 2
  echo "Testing ${_var}..."
  local _actual=$(condor_config_val ${_var} "$@")
  [[ "${_actual}" = ${_value} ]] && {
    echo -e "  \x1B[92mPASS\x1B[0m"
  } || {
    echo -e "  \x1B[91mFAIL\x1B[0m | '${_actual}' != '${_value}'"
    ((failures+=1))
  }
}

# -- test specific config values

# igwn-htcondor-config-accounting
assert_condor_config_val "CLASSAD_USER_MAPFILE_ValidSearchTags" "/etc/condor/accounting/valid_tags"
assert_condor_config_val "CLASSAD_USER_MAPFILE_ValidSearchUsers" "/etc/condor/accounting/valid_users"

# igwn-htcondor-config-vault
assert_condor_config_val "CREDMON_OAUTH" "/usr/sbin/condor_credmon_vault"
assert_condor_config_val "SEC_CREDENTIAL_GETTOKEN_OPTS" "*vault.ligo.org*"

# -- exit

if [ "${failures}" -ne 0 ]; then
  exit 1
fi
